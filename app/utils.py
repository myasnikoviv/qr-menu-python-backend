from app.db_connect import DbConnect
from app.alchemy_models import Menus, MenuItems, Restaurant, MenuComponents, Icons, FoodTags
from typing import List, Optional

dbc = DbConnect()
s = dbc.session()  # return session from SQLAlchemy


# Database query functions

# Return Menu list from Data Base (list of dict)
def get_menu_list(id_: int, name: Optional[str] = None):
    menulist = []
    for row in s.query(Menus).filter(Menus.restaurant_id == id_):
        tempdict = dict()
        tempdict["id"] = row.id
        tempdict["name"] = row.name
        tempdict["description"] = row.description
        # tempdict["connection id"] = id(s)
        menulist.append(tempdict)

    return menulist


# Return Menu Items list from Data Base (list of dict)
def get_menu_items_list(id_: int, name: Optional[str] = None):
    menulist = []
    for row in s.query(MenuItems).filter(MenuItems.restaurant_id == id_):
        tempdict = dict()
        tempdict["id"] = row.id
        tempdict["name"] = row.name
        tempdict["description"] = row.description
        tempdict["components"] = row.components
        tempdict["food_tags"] = row.food_tags
        tempdict["picture"] = row.picture
        tempdict["order"] = row.order
        tempdict["price"] = row.price
        # tempdict["connection id"] = id(s)
        menulist.append(tempdict)

    return menulist


# Return Menu Components list from Data Base (list of dict)
def get_menu_components_list():
    complist = []
    for row in s.query(MenuComponents):  # without filters for now
        tempdict = dict()
        tempdict["id"] = row.id
        tempdict["name"] = row.name
        # tempdict["connection id"] = id(s)
        complist.append(tempdict)

    return complist


# Return Menu Components list from Data Base (list of dict)
def get_menu_icons_list():
    iconslist = []
    for row in s.query(Icons):  # without filters for now
        tempdict = dict()
        tempdict["id"] = row.id
        tempdict["icon"] = row.icon
        tempdict["food_tags_id"] = row.food_tags_id
        # tempdict["connection id"] = id(s)
        iconslist.append(tempdict)

    return iconslist


# Return Menu Components list from Data Base (list of dict)
def get_food_tags_list():
    tagslist = []
    for row in s.query(FoodTags):  # without filters for now
        tempdict = dict()
        tempdict["id"] = row.id
        tempdict["tag"] = row.tag
        # tempdict["connection id"] = id(s)
        tagslist.append(tempdict)

    return tagslist
