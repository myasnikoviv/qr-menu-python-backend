from fastapi import FastAPI  # , HTTPException
from fastapi.exceptions import RequestValidationError
from starlette.responses import JSONResponse
from typing import List, Optional
from app.utils import get_menu_list, get_menu_items_list, get_menu_components_list, \
    get_menu_icons_list, get_food_tags_list

app = FastAPI()


@app.exception_handler(RequestValidationError)
def validation_exception_handler(request, exc):
    return JSONResponse({"staus_code": 200,
                         "id": "must be integer",
                         "name": "must be str"}, status_code=400)


# building "end points" for every request from Frontend
@app.get('/menus')  # getMenuList
def get_menulist(id: int, name: Optional[str] = None):
    if get_menu_list(id) == []:
        return JSONResponse({"staus_code": 200,
                             "id": "There is no restaurant with this id"},
                            status_code=200)
    else:
        return get_menu_list(id)


@app.get('/menu-items')  # getMenuItemsList
def get_menu_itemslist(id: int, name: Optional[str] = None):
    return get_menu_items_list(id)


@app.get('/menu-components')  #getMenuComponentsList
def get_menu_componentslist():  # without key for filtration, for now
    return get_menu_components_list()


@app.get('/menu-icons')  #getMenuIconsList
def get_menu_iconslist():  # without key for filtration, for now
    return get_menu_icons_list()


@app.get('/food-tags')  #getFoodTagsList
def get_food_tagslist():  # without key for filtration, for now
    return get_food_tags_list()
