from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


# Creating classes that display data in the tables of the database used(scheme of database)
class Restaurant(Base):
    __tablename__ = 'restaurants'

    id = Column(Integer, primary_key=True)
    title = Column(String(250), nullable=False)
    description = Column(String(250))


class Menus(Base):
    __tablename__ = 'menus'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    description = Column(String(250))
    depth = Column(Integer)
    order = Column(Integer)
    parent_id = Column(Integer)
    restaurant_id = Column(Integer, ForeignKey("Restaurant.id"))


class MenuItems(Base):
    __tablename__ = 'menu_items'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    description = Column(String(250))
    components = Column(String(250))
    food_tags = Column(String(250))
    picture = Column(String(250))
    order = Column(Integer)
    price = Column(Integer)
    restaurant_id = Column(Integer, ForeignKey("Restaurant.id"), nullable=False)
    menus_id = Column(Integer, ForeignKey("Menus.id"), nullable=False)


class MenuComponents(Base):
    __tablename__ = 'menu_components'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)


class Icons(Base):
    __tablename__ = 'icons'

    id = Column(Integer, primary_key=True)
    icon = Column(String(250), nullable=False)
    food_tags_id = Column(Integer, ForeignKey("Food_tags.id"))


class FoodTags(Base):
    __tablename__ = 'food_tags'

    id = Column(Integer, primary_key=True)
    tag = Column(String(250), nullable=False)
