from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.credentials import *


# The class for connection to DataBase
class DbConnect():
    __instance = None
    __credentials = credentials_db

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super().__new__(cls)
        return cls.__instance

    def __init__(self):
        if self.__dict__ == {}:
            engine = create_engine(self.__credentials, echo=True)
            self.session = sessionmaker(bind=engine)

    def __del__(self):
        DbConnect.__instance = None

    def connection_id(self):
        return id(self.session)