from setuptools import setup

setup(
    name='qrcodemenu_backend',
    version='0.0.1',
    install_requires=[
        'requests',
        'importlib; python_version == "2.6"',
    ],
)


